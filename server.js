const express = require('express')
const bodyParser = require('body-parser')
const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

const resultado = []

function fibonacci(num) {
    if (num === 0 || num === 1) {
        return num
    }
    return fibonacci(num - 1) + fibonacci(num - 2)
}

app.get('/:number', (req, res) => {
    const result = fibonacci(req.params.number)
    res.status(200)
    return res.send(result)
})



app.listen(5050, () => console.log('Listening on port 5050'))