const chai = require('chai')
const http = require('chai-http') // Extensão da lib para simular a requisição
const expect = chai.expect

const server = require('../server') //Arquivo que será testado
chai.use(http)


describe('Teste das funcoes', () => {

    it('sendNumber - GET', () => {

        chai.request('http://localhost:5050')
            .get('/')
            .query('10')
            .end(function (err, res) {
                chai.expect(err).to.be.null;
                chai.expect(res).to.have.status(200);
                chai.expect(res).to.be.equal(55);
            })
    })

})